before:
  test.nop


# trying to refer a state which is defined in a different file.

#!require:succeed_with_changes_state_1

{% set nested_value = hub.idem.arg_bind.resolve("${test:succeed_with_changes_state_1:testing:old}") %}
test_arg_bind_jinja_3:
  test.succeed_with_comment:
    - comment: {{ nested_value }}
#!END

#!require:succeed_with_changes_state_2

{% set nested_value = hub.idem.arg_bind.resolve("${test:succeed_with_changes_state_2:testing:new}") %}
test_arg_bind_jinja_4:
  test.succeed_with_comment:
    - comment: {{ nested_value }}
#!END
