test_include_sls_run_2:
  test.present:
    - resource_id: idem-test-2
    - new_state:
        key: {{ params["key"] }}
        arg_bind: {{ params["arg_bind"] }}
        run_level_param: {{ params["run_level_param-2"] }}
        resource_id: idem-test-2
        arg_bind_group: {{ params["key"] }}
        db_parameter_group_name: {{ params["db_parameter_group_name"] }}
    - result: true
