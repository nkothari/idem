State {{ params.get('state').get('name') }} Present:
  test.nop:
    - name: {{ params.get('state').get('name') }}
    - parameters:
        location: {{ params['invalid'][0] }}
        backup_location: {{ params['locations'][1] }}
