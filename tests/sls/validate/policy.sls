{% set tag_key_value = params.get('tag_key_value') %}
Some Policy:
  aws.organizations.policy.present:
  - content: |
       {% for tag in tag_key_value %}
          {% for key, value in tag.items() %}
              {
                "tag_value": [{% for v in value.split(',') %}"{{v}}"{% if loop.last %}{% else %},{% endif %}{% endfor %}]
              }
          {% endfor %}
      {% endfor %}
