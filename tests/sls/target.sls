the_target:
  test.succeed_without_changes:
    - require:
      - test: dep_1

not_executed_1:
  test.nop

not_executed_2:
  test.nop

dep_1:
  test.nop:
  - require:
    - test: dep_2

dep_2:
  test.nop

another_target:
  test.succeed_without_changes:
    - name: the_name

nested_resources:
  test.succeed_without_changes:
    - name: test_nested
  test_regex.none_without_changes_regex:
    - require:
      - test: dep_1
