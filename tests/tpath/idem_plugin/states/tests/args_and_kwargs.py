"""
This plugin existing tests the signature of the resource contract
"""
__contracts__ = ["resource"]


def present(hub, ctx, name: str, required, not_required=None):
    ...


def absent(hub, ctx, name: str, required, not_required=None):
    ...


async def describe(hub, ctx):
    ...
