import unittest.mock as mock
from typing import Iterable

import pop.hub


def test_params_default():
    """
    Verify that the default params are an iterable
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state", "test.sls"]):
        hub.pop.config.load(["idem"], "idem", parse_cli=False)
    assert isinstance(hub.OPT.idem.params, Iterable)


def test_progress_bar():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state", "test.sls", "--progress"]):
        hub.pop.config.load(["idem", "rend", "evbus"], "idem")

    assert hub.OPT.idem.progress is True


def test_no_progress_bar():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state", "test.sls", "--no-progress-bar"]):
        hub.pop.config.load(["idem", "rend", "evbus"], "idem")

    assert hub.OPT.idem.progress is False


def test_progress_bar_default():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state", "test.sls"]):
        hub.pop.config.load(["idem", "rend", "evbus"], "idem")

    assert hub.OPT.idem.progress is True
