import pytest


@pytest.mark.asyncio
async def test_no_reconcile(hub, code_dir, tmpdir):
    reconciler = "none"

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0
    assert ret["require_re_run"] is False


@pytest.mark.asyncio
async def test_basic_reconcile_no_reruns(hub, mock_hub, code_dir, tmpdir):
    reconciler = "basic"
    # Set up the hub like idem does

    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop

    hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_|-state name_|-name": {
                    "changes": {},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "result": True,
                }
            }
        }
    }

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0
    assert ret["require_re_run"] is False


@pytest.mark.asyncio
async def test_basic_reconcile_exec_not_pending(hub, mock_hub, code_dir, tmpdir):
    # Skipping reconciliation for exec if the result is 'True'
    # Since exec will always have 'changes' but no need to reconcile
    reconciler = "basic"

    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop

    hub.idem.RUNS = {
        "test": {
            "running": {
                "exec_|-state name_|-state name_|-name": {
                    "changes": {},
                    "comment": "exec.run",
                    "name": "state.name",
                    "result": True,
                }
            }
        }
    }

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tmpdir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0
    assert ret["require_re_run"] is False


@pytest.mark.asyncio
async def test_basic_reconcile_reruns(hub, mock_hub, code_dir, tmpdir):
    # Test reruns. One state require reconcile, the other does not.
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tmpdir

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.get_pending_tags = hub.reconcile.basic.get_pending_tags
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name1_|-state name1_|-name1": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name1",
                    "result": True,
                },
                "test.succeed_without_changes_|-state name2_|-state name2_|-name2": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {},
                    "comment": "No changes. No reconcile",
                    "name": "state.name2",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert (
            "test.succeed_without_changes_|-state name1_|-state name1_|-name1"
            in pending_tags
        )
        assert len(pending_tags) == 1

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            acct_blob=None,
            subs=[],
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3
    assert ret["require_re_run"] is True


@pytest.mark.asyncio
async def test_reconcile_rerun_accumulative_changes_with_metadata(
    hub, mock_hub, code_dir, tmpdir
):
    # this test verifies that the reconciliation loop updates the 'old_state'
    # In this case there is no change old_state and therefore 'changes' are not overridden.
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tmpdir

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.get_pending_tags = hub.reconcile.basic.get_pending_tags
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.reconcile.basic.update_result = hub.reconcile.basic.update_result
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_A_|-state name_|-name": {
                    "old_state": {"hello": "A", "world": "A"},
                    "new_state": {"hello": "A1", "world": "A1"},
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "rerun_data": "reconciler-metadata-A",
                    "result": True,
                },
                "test.succeed_without_changes_|-state name_B_|-state name_|-name": {
                    "old_state": {"hello": "B", "world": "B"},
                    "new_state": {"hello": "B1", "world": "B1"},
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "rerun_data": "reconciler-metadata-B",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert len(pending_tags) == 2

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            subs=["states"],
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_blob=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3
    assert ret["require_re_run"] is True
    run_state_A = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_A_|-state name_|-name"
    ]
    run_state_B = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_B_|-state name_|-name"
    ]
    assert run_state_A
    assert run_state_A["changes"]
    assert run_state_B["changes"]
    assert {"change": "is constant"} == run_state_A["changes"]
    assert {"change": "is constant"} == run_state_B["changes"]

    assert run_state_A["rerun_data"]
    assert run_state_B["rerun_data"]


def test_populate_comments(hub):
    # Check run with comments of different types
    run_1 = {
        "tag1": {"comment": "str_comment"},
        "tag2": {"comment": ["list_comment_elem1", "list_comment_elem2"]},
        "tag3": {
            "comment": (
                "tuple_elem1",
                "tuple_elem2",
                "tuple_elem3",
            )
        },
        "tag4": {"comment": {"dic_comment": "dic_value"}},
    }

    tag_to_comments = {}
    tag_to_comments = hub.reconcile.basic.populate_comments(run_1, tag_to_comments)
    assert tag_to_comments
    assert tag_to_comments.get("tag1") == ("str_comment",)
    assert tag_to_comments.get("tag2") == (
        "list_comment_elem1",
        "list_comment_elem2",
    )
    assert tag_to_comments.get("tag3") == (
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
    )
    assert not tag_to_comments.get("tag4")

    # Concatenate a second run
    run_2 = {
        "tag1": {"comment": {"dic_comment": "dic_value"}},
        "tag2": {
            "comment": (
                "tuple_elem1",
                "tuple_elem2",
                "tuple_elem3",
            )
        },
        "tag3": {"comment": ["list_comment_elem1", "list_comment_elem2"]},
        "tag4": {"comment": "str_comment"},
    }
    tag_to_comments = hub.reconcile.basic.populate_comments(run_2, tag_to_comments)
    assert tag_to_comments.get("tag1") == ("str_comment",)
    assert tag_to_comments.get("tag2") == (
        "list_comment_elem1",
        "list_comment_elem2",
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
    )
    assert tag_to_comments.get("tag3") == (
        "tuple_elem1",
        "tuple_elem2",
        "tuple_elem3",
        "list_comment_elem1",
        "list_comment_elem2",
    )
    assert tag_to_comments.get("tag4") == ("str_comment",)


def test_reconcile_with_require(idem_cli, tests_dir, mock_time):
    # Test reconciliation where the pending resource has a dependency
    # In this case since the required is not a resource in ESM
    # it will be re-executed as well
    # Re-runs due to reconciliation are verified by concatenated 'rerun_data'
    output = idem_cli("state", tests_dir / "sls" / "reconcile.sls", check=True).json

    assert 2 == len(output), len(output)
    # none_without_changes_with_rerun_data concatenates `WO_RERUN-` to rerun_data for each execution
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"]["result"]
        is True
    )
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"].get(
            "rerun_data"
        )
        is not None
    )
    assert (
        output["test_|-good_|-good_|-succeed_without_changes_with_rerun_data"][
            "rerun_data"
        ]
        == "WO_RERUN-WO_RERUN-WO_RERUN-WO_RERUN-"
    )

    # succeed_with_changes concatenates `W_RERUN-` to rerun_data for each execution
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"][
            "result"
        ]
        is True
    )
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"].get(
            "rerun_data"
        )
        is not None
    )
    assert (
        output["test_|-change_1_|-change_1_|-succeed_with_changes_with_rerun_data"][
            "rerun_data"
        ]
        == "W_RERUN-W_RERUN-W_RERUN-W_RERUN-"
    )


def test_reconcile_with_exec_and_rerun_data(idem_cli, tests_dir, mock_time):
    # Test reconciliation of exec.run
    # For reconciliation of exec.run the result should be 'False'
    # Re-runs due to reconciliation are verified by concatenated 'rerun_data'
    output = idem_cli(
        "state", tests_dir / "sls" / "reconcile_exec.sls", check=True
    ).json

    assert 1 == len(output), len(output)
    assert output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"]["result"] is False
    assert (
        output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"].get("rerun_data")
        is not None
    )
    assert (
        output["exec_|-exec_with_rerun_|-exec_with_rerun_|-run"]["rerun_data"]
        == "-1-1-1-1-"
    )
