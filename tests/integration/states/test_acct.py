import pytest
from pytest_idem.runner import run_yaml_block


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_basic(__test):
    """
    Add a new profile to the dictionary under the "test" provider.
    Verify that its kwargs constitute the new profile
    """
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - key_1: value_1
        - key_2: value_2

    test_result:
      tst_ctx.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state, test=__test)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }
    assert ret["tst_ctx_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_duplicate(__test):
    """
    Verify that adding a duplicate profile results in a failure
    """
    state = """
    new_profile:
      acct.profile:
        - provider_name: test

    duplicate_profile:
      acct.profile:
        - profile_name: new_profile
        - provider_name: test
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state, test=__test)
    assert (
        "Overwriting 'new_profile' under provider 'test'"
        in ret["acct_|-duplicate_profile_|-duplicate_profile_|-profile"]["comment"]
    )


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_processed(__test):
    state = """
    processed_profile:
      acct.profile:
        - provider_name: test.mod
        - key_1: value_1

    processed_result:
      tst_ctx.acct:
        - acct_profile: processed_profile
        - require:
            - acct: processed_profile
    """

    ret = run_yaml_block(state, test=__test)

    state_ret = ret["tst_ctx_|-processed_result_|-processed_result_|-acct"]

    # Verify that a provider name of "test.mod" gets modified by the tpath acct plugin
    assert state_ret["result"] is True, state_ret["comment"]
    assert state_ret["new_state"] == {
        "key_1": "value_1",
        "modded": True,
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_unprocessed(__test):
    state = """
    unprocessed_profile:
      acct.profile:
        - provider_name: test
        - key_1: value_1

    unprocessed_result:
      tst_ctx.acct:
        - acct_profile: unprocessed_profile
        - require:
            - acct: unprocessed_profile
    """
    ret = run_yaml_block(state, test=__test)

    # Verify that a provider name of "test" is not modified by the tpath acct plugin
    assert (
        ret["acct_|-unprocessed_profile_|-unprocessed_profile_|-profile"]["result"]
        is True
    )
    assert ret["tst_ctx_|-unprocessed_result_|-unprocessed_result_|-acct"][
        "new_state"
    ] == {"key_1": "value_1"}


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_acct_data(__test):
    """
    Verify that a one-off profile works
    """
    state = """
    test_result:
      tst_ctx.acct:
        - acct_profile: new_profile
        - acct_data:
            profiles:
              test:
                new_profile:
                  key_1: value_1
                  key_2: value_2
    """
    ret = run_yaml_block(state, test=__test)
    assert ret["tst_ctx_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_arg_bind(__test):
    state = """
    mock_acct:
      test.present:
        - new_state:
            key_1: value_1
            key_2: value_2

    new_profile:
      acct.profile:
        - provider_name: test
        - key_1: ${test:mock_acct:key_1}
        - key_2: ${test:mock_acct:key_2}

    test_result:
      tst_ctx.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state, test=__test)

    # Verify that the present state ran correctly
    assert ret["test_|-mock_acct_|-mock_acct_|-present"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }

    # Verify that the argbinding to the new profile was successful
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }

    # Verify that the credentials profile is available in the new state
    assert ret["tst_ctx_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_copy_from_new_acct_data(__test):
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - acct_data: {"profiles": {"test": {"source": {"key_1": "overwritten", "key_3": "copied"}}}}
        - source_profile: source
        - key_1: value_1
        - key_2: value_2

    test_result:
      tst_ctx.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state, test=__test)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }
    assert ret["tst_ctx_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": "copied",
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_copy_from_existing_acct_data(__test):
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - source_profile: source
        - key_1: value_1
        - key_2: value_2

    test_result:
      tst_ctx.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    acct_data = {
        "profiles": {"test": {"source": {"key_1": "overwritten", "key_3": "copied"}}}
    }
    ret = run_yaml_block(state, acct_data=acct_data, test=__test)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["source", "new_profile"]},
        "old": {"profiles": ["source"]},
    }
    assert ret["tst_ctx_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": "copied",
    }


@pytest.mark.parametrize("__test", [True, False], ids=["--test", "+"])
def test_fail(__test):
    """
    Verify that an error in gathering mods makes the acct.profile state fail and states that use it fail
    """
    state = """
    failed_profile:
      acct.profile:
        - provider_name: test.bad

    test_result:
      tst_ctx.acct:
        - acct_profile: failed_profile
        - require:
            - acct: failed_profile
    """
    ret = run_yaml_block(state, test=__test)

    for name, state_ret in ret.items():
        assert state_ret["result"] is False, state_ret["comment"]
