import fnmatch
import os
import pathlib

import msgpack
import pytest_idem.runner as runner
import yaml


def test_cli(tmpdir, idem_cli):
    run_name = "test_restore"
    # Make sure it goes to the cache_file from the "local" plugin, the other cache_file is rightfully gone
    cache_file = tmpdir / "esm" / "local" / f"{run_name}.msgpack"

    with runner.named_tempfile(suffix=".msgpack", delete=True) as fh:
        fh.write_text('{"1":"2"}')

        with runner.named_tempfile(suffix=".cfg", delete=True) as cfg_fh:
            data = yaml.safe_dump({"idem": {"esm_keep_cache": True}})
            cfg_fh.write_text(data)

            # Run the restore command
            idem_cli(
                "restore",
                fh,
                f"--cache-dir={tmpdir}",
                f"--run-name={run_name}",
                f"--config={cfg_fh}",
                check=True,
            )

    # The data should have been transferred to the cache_file
    with cache_file.open("rb") as fh:
        contents = msgpack.load(fh)
        contents.pop("__esm_metadata__")
        assert contents == {"1": "2"}


def _find_file(dir_path: pathlib.Path, pattern: str) -> pathlib.Path:
    for root, dirs, files in os.walk(top=dir_path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return dir_path / name
