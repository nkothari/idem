def test_version(idem_cli, hub):
    ret = idem_cli("exec", "esm.version")
    assert ret.json.result is True
    assert ret.json.ret == ".".join(str(x) for x in hub.esm.VERSION)
