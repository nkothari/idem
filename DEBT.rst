TECH DEBT
=========

Sometimes a feature is needed quickly, but needs various refactors for long-term maintainability.

- `idem/state.py` needs to be made into a directory with files containing the smaller parts I.E.
    - idem/state/init.py
    - idem/state/compile.py
    - idem/state/batch.py
    - idem/state/run.py
- The `params` feature needs to be pluggable, and possibly be made into a separate project
- idem at the core, is a *language* and states/exec non-generic modules should exist in side projects
- esm/local should be a separate project
