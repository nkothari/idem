.. idem documentation master file, created by
   sphinx-quickstart on Wed Feb 20 15:36:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :maxdepth: 3
   :glob:
   :hidden:

   topics/config
   topics/doc
   topics/extending
   topics/add_requisites
   topics/sls_meta
   topics/sls_structure
   topics/parameters
   topics/params_validation
   topics/argument_binding
   topics/sls_inversion
   topics/jmespath
   topics/transparent_req
   topics/acct
   topics/acct_file
   topics/ignore_changes_requisite
   topics/recreate_on_update_requisite
   topics/jinja_arg_bind_delayed_rend
   topics/jinja_sandboxing
   topics/delayed_rendering
   topics/sensitive_requisite
   topics/sls_acct
   topics/sls_sources
   topics/sls_tree
   topics/sls_exec
   topics/sls_resolver_plugins
   topics/sls_run
   topics/implied_sources
   topics/group_plugins
   topics/reconciliation_loop
   topics/enforced_state_management
   topics/esm_versioning
   topics/progress_bar
   topics/count
   topics/events
   topics/kubernetes_crd
   topics/scripts
   topics/describe
   topics/helper_functions/index
   topics/single_target

   topics/azure_docs
   topics/migrate_salt
   releases/index

.. toctree::
   :caption: Get involved
   :maxdepth: 2
   :glob:
   :hidden:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/idem/idem>

.. toctree::
   :caption: Links
   :hidden:

   Idem Project Docs Portal <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Source on GitLab <https://gitlab.com/vmware/idem>
   POP Project Source on GitLab <https://gitlab.com/vmware/pop>
