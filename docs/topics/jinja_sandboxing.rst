=============
Jinja Sandbox
=============

To evaluate untrusted code on an Idem server, Idem enables the `Jinja sandbox <https://jinja.palletsprojects.com/en/3.0.x/sandbox>`_ by default.

If you need to disable the Jinja sandbox, use one of the following options.

On the CLI:

.. code-block:: bash

    idem state --disable-jinja-sandbox

In the Idem config file:

.. code-block:: yaml

    # idem.cfg
    rend:
      enable_jinja_sandbox: False

Safe Refs
=========

The Jinja sandbox only allows *hub* references to be called if they are on the *jinja_sandbox_safe_hub_refs* allow list.

By default, only functions under ``hub.exec`` are enabled. To enable other references on the *hub*, add them to the Idem config file.

.. code-block:: yaml

    rend:
      jinja_sandbox_safe_hub_refs:
        # Enable everything under hub.exec
        - "exec.*"
        # Enable everything under hub.tool
        - "tool.*"
        # Enable a specific full reference
        - "my_sub.my_func"
