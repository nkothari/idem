===============================
SLS Run state
===============================

sls.run state in idem  is used to structure a group of SLS files into an independent construct that can be invoked from SLS code
multiple times with different set of arguments and parameters
sls.run expects sls_sources, params and inline parameters as input.

* sls_sources(list): List of sls files to run.
* params(list, Optional): List of params files to be used in resolving sls files.
* inline params can be provided

The sls.run retrieves all states from the provided sls_sources and resolves the params with the inline parameters, params files provided in sls.run,
and parameters provided during run. Once all the parameters are resolved, the resolved states are added to
the existing run.

The order of precedence for parameters is inline parameters (parameters provided in sls.run) , parameters file in sls.run,
parameters file provided during run.

**Example**

In the below example we are creating 3 networks by calling the same network.sls file three times with different network names.

If we want to create a new network in future we can add one more sls.run with the new network name. If we want to add
more instances to all networks we just need an instance in network.sls.

In the example 3 types of parameters are used.

**Inline parameters**  network_name: Network_A

**parameter files in sls.run** file1

**parameter file provided during initial run** The parameter file we give when we run the state.

.. code-block:: sls

    Network_A:
      sls.run:
        - sls_sources:
            - sls.network
        - params:
            - params.file1
        - network_name: Network_A

    Network_B:
      sls.run:
        - sls_sources:
            - sls.network
        - params:
            - params.file2
        - network_name: Network_B

    Network_C:
      sls.run:
        - sls_sources:
            - sls.network
        - params:
            - params.file3
        - network_name: Network_C

We can have sls.run inside each included files.

Example-:
network.sls
.. code-block:: sls

    State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_address: "${cloud:State_B:nics[0]:address}"

    State_B:
      cloud.instance.present:
        - name: "Instance B"
        - nics:
            - network_name: "Network_1"
              # address is populated after state is executed
              address:
            - network_name: "Network_2"
              # address is populated after state is executed
              address:

    # nested sls.run

    State_C:
      sls.run:
        - sls_sources:
            - sls.instances
        - params:
            - params.file3
        - instance_tag: cluster-autoscale


**Argument binding**

To arg bind values in sls.run included files, we should append sls:<sls.run state name> to the argument binding.
example-: ${sls:Network_A:cloud:State_B:nics[0]:address}

sls:network1 refers to sls.run state, cloud:State_A refers to state we are referring inside
included files, nics[0]:address refers to the attribute we are referring in that included state.

.. code-block:: sls

     State_A:
      cloud.instance.present:
        - name: "Instance A"
        - state_B_address: "${cloud:State_B:nics[0]:address}"


In the below state we are using argument binding from included file.

.. code-block:: sls

    arg_bind_sls_run:
      test.present:
        - result: True
        - changes: 'skip'
        - new_state:
            "network_A_address": ${sls:Network_A:cloud:State_B:nics[0]:address}
            "network_B_address": ${sls:Network_B:cloud:State_B:nics[0]:address}
