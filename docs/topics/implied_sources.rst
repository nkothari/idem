===============
Implied Sources
===============

You might want to set up a filesystem in which to store SLS and parameter files, but Idem needs to know where to look for those files. References to your SLS and parameter files are relative to a source location that you define.

If you don't define a source location, Idem uses the directory containing the Idem config file as the default location.

Tree
====

To specify an SLS and parameter file source location (the SLS root) do one of the following:

* Add a ``tree`` setting in the Idem config file.

* Use the ``--tree`` CLI option.

  The ``--tree`` CLI option only works for a root location on the same local machine as the Idem runtime. Remote locations are not supported.

  The ``--tree`` CLI option needs both SLS and parameter files to be under a single root on the local filesystem.

In the following command, *my-sls* can represent a file path with directories separated by dots. The path is relative to the SLS root specified with ``--tree``.

.. code-block:: bash

    idem state my-sls --tree "file://<path/to/SLS/root>"

Given an SLS root of ``/srv/idem`` and *my-sls* of ``/srv/idem/my-project/init.sls`` you can specify the command in any of the following equivalent ways.

.. code-block:: bash

    idem state my-project.init.sls --tree "file:///srv/idem"

    idem state my-project.init --tree "file:///srv/idem"

    idem state my-project --tree "file:///srv/idem"


* A final *.sls* file extension is assumed and doesn't need to be explicitly added.

* An final *init.sls* file after the directory path is assumed and doesn't need to be explicitly added. (If your SLS file isn't named *init* you must specify it though.)

  An *init.sls* file is analogous to an assumed *index.html* file at the end of a URL.

SLS Sources
===========

If you have multiple SLS roots or remote SLS roots, use the ``--sls-sources`` CLI option instead.

Idem checks SLS sources in left-to-right order as supplied on the CLI. If identical SLS file names occur under different root sources, Idem only uses the first match.

.. code-block:: bash

    idem state my-project --sls-sources "file:///srv/idem" "file:///srv/other-root"

Direct References
=================

You don't have to use ``--tree`` or ``--sls-sources``. Instead, you can include the SLS root directly in the path to your SLS files.

For ``dir1`` and ``dir2`` under the ``sls_root``, the following CLI commands are eqivalent.

.. code-block:: bash

    idem state dir1 dir2 --tree "file://sls_root"

    idem state dir1 dir2 --sls-sources "file://sls_root"

    idem state sls_root/dir1/init.sls sls_root/dir2/init.sls

Idem Config File
================

If you don't define an SLS root, Idem uses the directory containing the Idem config file as the default SLS root.

In the following example, ``/absolute/config/path`` becomes the SLS root.

.. code-block:: bash

    idem state --config /absolute/config/path/idem.cfg

Dots in File Names
==================

Because SLS path references are dot delimited, file names that include dots require special formatting.

Replace any base file name dots with a null ("\\0") character. You don't need a null for the .sls extension part, nor for directory separator dots.

In the following example, the SLS root is ``/srv/idem`` with directory ``my-dir`` under it, in which there is file ``file.name.with.dots.sls``.

To reference the file in an ``include`` block:

.. code-block:: sls

    include:
      - "my-dir.file\0name\0with\0dots"

To reference the file in the CLI:

.. code-block:: bash

    idem state "my-dir.file\0name\0with\0dots" --sls-sources /srv/idem

    $ idem state foo bar --tree "file://sls_root"


Special Cases
-------------

SLS references are dot delimited.
If the SLS reference refers to a file path that contains "." in the file name (not counting the ".sls" suffix) or in one of the directory names,
then a null character needs to be used in the reference in place of dots

For example, consider an sls source at "/srv/idem".
If an sls file named "file.name.with.dots.sls" is in /srv/idem/nest,
then it's reference would be "nest.file\0name\0with\0dots"

It would be used in include statements like so:

.. code-block:: sls

    include:
      - "nest.file\0name\0with\0dots"

It would be called from the cli like so:

.. code-block:: bash

    idem state "nest.file\0name\0with\0dots" --sls-sources /srv/idem
